package Main;

import com.jme3.app.SimpleApplication;
import com.jme3.cursors.plugins.JmeCursor;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.CameraNode;
import com.jme3.scene.control.CameraControl.ControlDirection;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Line;
import com.jme3.scene.Spatial;
import com.jme3.system.AppSettings;

public class Main extends SimpleApplication implements ActionListener{
    private static final float PI_2 = (float)(Math.PI / 2);
    
    private float toadSpeed = 0.2f;
    private static Vector2f screenCenter;
    
    private Node toadRotationNode = new Node();
    private Node toadNode = new Node();
    private boolean toadUp = false, toadDown = false, toadLeft = false, toadRight = false, rightMouse = false;
    private Node laserNode = new Node();
    private Geometry laserGeo;
    
    public static void main(String[] args){
        AppSettings settings = new AppSettings(true);
        settings.setRenderer(AppSettings.LWJGL_OPENGL1);
        Main app = new Main();
        app.setSettings(settings);
        screenCenter = new Vector2f(app.settings.getWidth() / 2, app.settings.getHeight() / 2);
        app.start(); //Start game
    }
    
    @Override
    public void simpleInitApp(){
        initLight();
        initShip();
        initToad();
        initLaser();
        initCamera();
        initCursor();
        initKeys();
    }
    
    private void initLight(){
        DirectionalLight sun = new DirectionalLight();
        sun.setColor(ColorRGBA.White);
        sun.setDirection(new Vector3f(-.5f,-.5f,-.5f).normalizeLocal());
        rootNode.addLight(sun);
    }
    
    private void initShip(){
        Spatial shipObj = assetManager.loadModel("Models/Ship/ship.obj");
        shipObj.setLocalScale(shipObj.getLocalScale().divideLocal(20f));
        shipObj.setLocalTranslation(0f, -0.5f, 0f);
        Material shipMat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        shipMat.setColor("Diffuse", ColorRGBA.LightGray);
        shipObj.setMaterial(shipMat);
        rootNode.attachChild(shipObj);
    }
    
    private void initToad(){
        Spatial toadObj = assetManager.loadModel("Models/Toad/Toad_Body_mesh_001.obj");
        toadObj.setLocalScale(toadObj.getLocalScale().divideLocal(30f));
        Material toadMat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        toadMat.setTexture("DiffuseMap", assetManager.loadTexture("Models/Toad/Toad_Diffuse_Map.tga"));
        toadMat.setTexture("SpecularMap", assetManager.loadTexture("Models/Toad/Toad_Specular_Map.tga"));
        toadMat.setTexture("NormalMap", assetManager.loadTexture("Models/Toad/Toad_Normal_Map.tga"));
        toadObj.setMaterial(toadMat);
        toadRotationNode.attachChild(toadObj);
        toadNode.attachChild(toadRotationNode);
        rootNode.attachChild(toadNode);
    }
    
    private void initLaser(){
        Line laserObj = new Line(toadNode.getLocalTranslation().add(0f, 0f, 0f), toadNode.getLocalTranslation().add(0f, 0f, 50f));
        laserGeo = new Geometry("Line", laserObj);
        Material laserMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        laserMat.setColor("Color", ColorRGBA.Red);
        laserGeo.setMaterial(laserMat);
        laserNode.setLocalTranslation(0f, 3.5f, 0f);
        toadRotationNode.attachChild(laserNode);
    }
    
    private void initCamera(){
        flyCam.setEnabled(false);
        CameraNode camNode = new CameraNode("Cam Node", cam);
        camNode.setControlDir(ControlDirection.SpatialToCamera);
        toadNode.attachChild(camNode);
        camNode.setLocalTranslation(new Vector3f(toadNode.getWorldTranslation().add(-5f, 15f, 0f)));
        camNode.lookAt(toadNode.getLocalTranslation(), Vector3f.UNIT_Y);
    }
    
    private void initCursor(){
        inputManager.setCursorVisible(true);
        inputManager.setMouseCursor((JmeCursor)assetManager.loadAsset("Misc/Cursors/cursor.cur"));
    }
    
    private void initKeys(){
        //You can map one or several inputs to one named action
        inputManager.addMapping("Up", new KeyTrigger(KeyInput.KEY_W));
        inputManager.addMapping("Down", new KeyTrigger(KeyInput.KEY_S));
        inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_A));
        inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping("RightMouse", new MouseButtonTrigger(MouseInput.BUTTON_RIGHT));
        //Add the names to the action listener.
        inputManager.addListener(this, "Up", "Down", "Left", "Right", "RightMouse");
    }
    
    @Override
    public void onAction(String binding, boolean keyPress, float tpf){
        if(binding.equals("Up"))toadUp = keyPress;
        if(binding.equals("Down"))toadDown = keyPress;
        if(binding.equals("Left"))toadLeft = keyPress;
        if(binding.equals("Right"))toadRight = keyPress;
        if(binding.equals("RightMouse"))rightMouse = keyPress;
    }
    
    @Override
    public void simpleUpdate(float tpf){
        if(toadUp)toadNode.setLocalTranslation(toadNode.getLocalTranslation().add(toadSpeed, 0f, 0f));
        if(toadDown)toadNode.setLocalTranslation(toadNode.getLocalTranslation().add(-toadSpeed, 0f, 0f));
        if(toadLeft)toadNode.setLocalTranslation(toadNode.getLocalTranslation().add(0f, 0f, -toadSpeed));
        if(toadRight)toadNode.setLocalTranslation(toadNode.getLocalTranslation().add(0f, 0f, toadSpeed));
        if(rightMouse)laserNode.attachChild(laserGeo);
        else laserNode.detachChild(laserGeo);
        mouseUpdate();
    }
    
    private void mouseUpdate(){
        Vector2f mousePos = inputManager.getCursorPosition();
        Vector2f relativeMousePos = new Vector2f(mousePos.subtract(screenCenter));
        //TODO:Rotation method is hacked, replace with proper translation of mouse position
        toadRotationNode.lookAt(toadRotationNode.getLocalTranslation().add(-relativeMousePos.x, 0, relativeMousePos.y), Vector3f.UNIT_Y);
        toadRotationNode.rotate(0, PI_2, 0);
    }
}